## Changelog livebuild_dflinuxe11 ##

###### 23/09/2024 - VERSION 11.11

 * Modification DFbuild.sh VERSION="11.11" [Dernière version oldstable Debian 11.11 Bullseye](https://www.debian.org/News/2024/2024021002 "onzième et dernière mise à jour de sa distribution oldstable Debian 11")
 * Modification sur /doc/dflinuxe11_desktop.ods et dflinuxe11_desktop.pdf
 * Modification sur config/includes.chroot/etc/skel/Documents/dflinuxe11_desktop.pdf

###### 11/02/2024 - VERSION 11.9
 * Modification DFbuild.sh VERSION="11.9" [version oldstable Debian 11.9 Bullseye](https://www.debian.org/News/2024/2024021002 "la neuvième mise à jour de sa distribution oldstable Debian 11")
 * Modification sur /doc/dflinuxe11_desktop.ods et dflinuxe11_desktop.pdf
 * Modification sur config/includes.chroot/etc/skel/Documents/dflinuxe11_desktop.pdf

###### 08/10/2023 - VERSION 11.8
 * Modification DFbuild.sh VERSION="11.8" [version oldstable Debian 11.8 Bullseye](https://www.debian.org/News/2023/2023100702 "la  huitième mise à jour de sa distribution oldstable Debian 11")
 * Modification sur /doc/dflinuxe11_desktop.ods et dflinuxe11_desktop.pdf
 * Modification sur config/includes.chroot/etc/skel/Documents/dflinuxe11_desktop.pdf
 * Mise à jour du fichier /Documents/tuto_miseajour.pdf
 * Mise à jour du fichier /doc/dflinuxe11_arborescence.txt

###### 30/04/2023  - VERSION 11.7
 * Modification DFbuild.sh VERSION="11.7" [version stable Debian 11.7 Bullseye](https://www.debian.org/News/2023/20230429 "la septième mise à jour de sa distribution stable Debian 11")
 * Modification sur /doc/dflinuxe11_desktop.ods et dflinuxe11_desktop.pdf
 * Modification sur config/includes.chroot/etc/skel/Documents/dflinuxe11_desktop.pdf

###### 17/12/2022 - VERSION 11.6
 * Modification DFbuild.sh VERSION="11.6" [version stable Debian 11.6 Bullseye](https://www.debian.org/News/2022/20221217 "la sixième mise à jour de sa distribution stable Debian 11")
 * Modification sur /doc/dflinuxe11_desktop.ods et dflinuxe11_desktop.pdf
 * Modification sur config/includes.chroot/etc/skel/Documents/dflinuxe11_desktop.pdf
 
###### 11/11/2022 - VERSION 11.5
 * Correction typo sur README.md
 * Correction  fichier /auto/config
 
###### 20/09/2022 - VERSION 11.5
 * Modification DFbuild.sh VERSION="11.5" [version stable Debian 11.5 Bullseye](https://www.debian.org/News/2022/2022091002 "la cinquième mise à jour de sa distribution stable Debian 11")
 * Modification sur /doc/dflinuxe11_desktop.ods et dflinuxe11_desktop.pdf
 * Modification sur config/includes.chroot/etc/skel/Documents/dflinuxe11_desktop.pdf
 
###### 12/07/2022 - VERSION 11.4
 * Modification DFbuild.sh VERSION="11.4" [version stable Debian 11.4 Bullseye](https://www.debian.org/News/2022/20220709 "la quatrième mise à jour de sa distribution stable Debian 11")
 * Modification sur /doc/dflinuxe11_desktop.ods et dflinuxe11_desktop.pdf
 * Modification sur config/includes.chroot/etc/skel/Documents/dflinuxe11_desktop.pdf
 
###### 26/03/2022 - VERSION 11.3
 * Modification DFbuild.sh VERSION="11.3"
 [version stable Debian 11.3 Bullseye](https://www.debian.org/News/2022/20220326 "la troisième mise à jour de sa distribution stable Debian 11")
 * Remplacement de dflinuxe_ par dle_ pour identifier la clef (version de la clef USB) 
 * Modification sur /doc/dflinuxe11_desktop.ods et dflinuxe11_desktop.pdf
 * Modification sur config/includes.chroot/etc/skel/Documents/dflinuxe11_desktop.pdf
 
###### 19/12/2021 - VERSION 11.2
 * Modification DFbuild.sh VERSION="11.2"
 [version stable Debian 11.2 Bullseye]( https://www.debian.org/News/2021/20211218 "la deuxième mise à jour de sa distribution stable Debian 11")
 * Remplacement de dflinuxe par dle et ajout de -${ARCH} après VERSION pour identifier la clef (version de la clef USB)
 * Suppression des options de déconnexion ShowHibernate (Mise en veille prolongée), ShowHybridSleep (mise en veulle hybride), ShowSwitchUser (changer d'utilisateur) dans .config/xfce4/xfconf/xfce-perchannel-xml/xfce4-session.xml
 * Modification sur /doc/dflinuxe11_desktop.ods et dflinuxe11_desktop.pdf
 * Modification de dflinuxe11_arborescence.txt
 * Modification sur config/includes.chroot/etc/skel/Documents/dflinuxe11_desktop.pdf
 
###### 10/10/2021 - VERSION 11.1
 * Modification DFbuild.sh VERSION="11.1"
 [version stable Debian 11.1 Bullseye](https://www.debian.org/News/2021/20211009 "la première mise à jour de sa distribution stable Debian 11")
 * Ajout de l'image bullseye_pil_poil.png dans /config/includes.chroot/etc/skel/Images/bullseye_pil_poil.png
 * Modification de dflinuxe11_arborescence.txt
 * Modification sur /doc/dflinuxe11_desktop.ods et dflinuxe11_desktop.pdf
 * Modification sur config/includes.chroot/etc/skel/Documents/dflinuxe11_desktop.pdf
 
###### 15/08/2021 - VERSION 11.0.0
 * Modification DFbuild.sh VERSION="11.0.0"
 * [Nouvelle version stable Debian 11.0 Bullseye](https://www.debian.org/News/2021/20210814 "Après deux ans, un mois et neuf jours de développement, le projet Debian est fier d'annoncer sa nouvelle version stable n° 11 (nom de code "Bullseye")
 * Modification sur /doc/dflinuxe11_desktop.ods et dflinuxe11_desktop.pdf 
 * Ajout du fichier dflinuxe11_desktop.pdf dans config/includes.chroot/etc/skel/Documents/
 
###### 02/08/2021  - VERSION 11.0-rc3
 * Modification DFbuild.sh VERSION="11.0-rc3"
 * [base installateur Debian Bullseye RC 3](https://www.debian.org/devel/debian-installer/News/2021/20210802 "Publication de l'installateur Debian Bullseye RC 3")
 * Modification de /config/package-lists/dflinuxe.list.chroot
 * Modification des fichiers /doc/dflinuxe11_desktop.ods et /doc/dflinuxe11_desktop.pdf
 * Modification dans /.config/geany/geany.conf de la ligne editor_font de Monospace 10 par DejaVu Sans 11
 * Modification sur les fichiers /config/includes.binary/boot/grub/grub.cfg et /config/includes.binary/isolinux/install.cfg (Installation semi-automatique sur TOUT le disque)
 * Nettoyage du dossier /.mozilla/firefox/dflinuxe.default
 * Modification de dflinuxe11_arborescence.txt
 * Correction typo sur /config/includes.binary/boot/grub/grub.cfg
 
###### 21/06/2021  - VERSION 11.0-rc2
 * Modification DFbuild.sh VERSION="11.0-rc2"
 * [base installateur Debian Bullseye RC 2](https://www.debian.org/devel/debian-installer/News/2021/20210614 "Publication de l'installateur Debian Bullseye RC 2")
 
###### 15/05/2021  - VERSION 11.0-rc1d
 * Modification DFbuild.sh VERSION="11.0-rc1d"
 * Modification /config/includes.chroot/usr/share/dflinuxe/sources.list
 * Ajout paquet imagemagick omis sur dflinux.list.chroot
 * Corrections sur dflinuxe11_desktop.ods pour les fichiers .desktop
 * Ajout du HTTPS sur fichier /config/includes.chroot/usr/share/dflinuxe/sources.list pour main, updates et modification [pour debian-security](https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information.fr.html#security-archive "5.1.3. Changement de l’organisation de l’archive security")
 * Modification sur le fichier /auto/config --security passe de false à true
 * Mise à jour du fichier dflinuxe11_arborescence.txt
 
###### 05/05/2021  - VERSION 11.0-rc1c
 * Modification DFbuild.sh VERSION="11.0-rc1c"
 * Modification du fichier /config/includes.chroot/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-power-manager.xml
 * Modification du fichier /config/includes.chroot/etc/skel/.config/dconf/user pour configuration de Mousepad
 * Ajout du dossier /config/includes.chroot/etc/skel/.config/geany/ et fichier geany.conf pour configuration de geany
 * Remplacement dans le dossier /Musique/ par HK_Danser_encore.mp3 avec information sur fichier HK_Danser_encore.txt (On vous offre la chanson http://www.saltimbanks.fr/danserencore.html)
 * Ajout dans le dossier /Documents/ d'un fichier tuto_miseajour.pdf concernant les mises à jour de la distribution.
 * Mise à jour du fichier dflinuxe11_arborescence.txt
 * Renommage du fichier dflinuxe11_desktop0.ods en dflinuxe11_desktop.ods

###### 02/05/2021  - VERSION 11.0-rc1b
 * Modification DFbuild.sh VERSION="11.0-rc1b"
 * [base installateur Debian Bullseye RC 1](https://www.debian.org/devel/debian-installer/News/2021/20210423 "Publication de l'installateur Debian Bullseye RC 1")
 * Ajout pour Firefox navigateur par défaut de firefox-esr.desktop dans le fichier /config/includes.chroot/etc/skel/.config/mimeapps.list
 * Modification pour Thunar des fichiers configuration accels.scm et uca.xml
 
###### 18/04/2021  - VERSION 11.0-a3b3c
 * Modification DFbuild.sh VERSION="11.0-a3b3c"
 * Suppression du fichier ls_bulleyes.png
 * Modification de doc/dflinuxe11_arborescence.txt
 * Correction sur /doc/dflinuxe11_desktop0.ods (+firmware-iwlwifi)
 * Ajout sur /config/package-lists/dflinuxe.list.chroot (+firmware-iwlwifi dans #nonfree)
 * Modification sur la position du texte en bas d'écran grub sur _UEFI_ sur le fichier theme.txt
 * Modification par ajout de "Personnalisée par BozoSoft" sur
   * fichier images /includes.binary/isolinux/ splash-grub.png, splash.png, 
   * fichier images /includes.chroot/usr/share/backgrounds/xfce/ Debian11-login-live.png et Debian11-login.png,
   * fichier image /includes.chroot/usr/share/backgrounds/xfce/debianr-by-atapaz.jpg
   * fichier texte Debian11_svg.txt (correction des ajouts)
 * Modification sur Thunar/Recherche modification fichier image de l'icône catfish de config/includes.chroot/etc/skel/.config/Thunar/uca.xml
 * Modification sur dateheure de /config/includes.chroot/etc/skel/.config/xfce4/panel/datetime-16.rc
 
###### 07/03/2021 - VERSION 11.0-a3b3
 * Modification DFbuild.sh VERSION="11.0-a3b3"
 * Modification de README.md : suppression de modifier installer_debian-installer (lilo is not available any more in Bullseye) et de NOTE au 04/07/2020 bug=961257 (Adding symlink for udeb data for bullseye)
 * Correction de /config/package-lists/dflinuxe.list.chroot pour le paquet chromium, fusesm, orage
 * Modification des images /includes.binary/isolinux/ splash-grub.png, splash.png
 * Modification de l'image /includes.installer/usr/share/graphics/logo_debian.png
 * Modification des images /includes.chroot/usr/share/backgrounds/xfce/ Debian11-login-live.png, Debian11-login.png, Debian11.svg, Debian11_svg.txt	
 * dans /config/includes.chroot/etc/skel/Musique/ suppression de free software song.ogg et ajout de 01-Roses_Intro.ogg + 01-Roses_Intro.txt
 * dans /config/includes.chroot/etc/skel/Vidéos/ suppression de DFLinux_installation_auto.ogv et ajout de Waterfall-37088.ogv, Waterfall-37088.mp4, Waterfall37088.txt

###### 17/12/2020 - VERSION 11.0-a3b2
 * Modification DFbuild.sh VERSION="11.0-a3b2"
 * Modification des images pour Debian10-login.png, Debian11-login-live.png, splash-grub.png par ajout du 11 (regular 18 gras blanc) après Debian
 * Modification de l'image pour splash.png par ajout du 11 (regular 14 regular blanc) après Debian 
 * Modification de l'image Debian11.svg par le homeworld_wallpaper_2560x1600
 * Modification du fichier Debian11_svg.txt
 * Modification de README.md pour détailler l'installation
 * Correction de /config/package-lists/dflinuxe.list.chroot pour le paquet grsync
 * Correction de /config/package-lists/dflinuxe.list.chroot pour supprimer les paquets chromium chromium-l10
 * Correction sur le fichier dflinuxe11_desktop0.ods pour le paquet grsync, mtpaint et chromium 
 
###### 07/12/2020 - VERSION 11.0-a3b1
 * Modification DFbuild.sh VERSION="11.0-a3b1"
 * [base installateur Debian Bullseye Alpha 3](https://www.debian.org/devel/debian-installer/News/2020/20201206 "Publication de l'installateur Debian Bullseye Alpha 3")
 * Allégement des fichiers asunder.desktop, FBReader.desktop dossier /config/includes.chroot/usr/share/applications/ 
 * Ajout et correction typo sur /config/includes.chroot/usr/share/applications/org.gnome.baobab.desktop
 * Remplacement des images pas Homeworld
   * du logo_debian.png
   * des splash-grub.png et splash.png
   * des login-live.png et login.png
   * du Debian11.svg
 * Modification de dflinuxe.list.chroot avec mtpaint
 
###### 26/10/2020 - VERSION 11.0-a2b4
 * Modification DFbuild.sh VERSION="11.0-a2b4"
 * Modification pour gestionnaire d'alimentation (Économie d'énergie du système "Quand inactif depuis" = 0) fichier xfce4-power-manager.xml
 * Simplification des raccourcis clavier de config/includes.chroot/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml
 * Ajout du fichier lightdm-gtk-greeter-settings.mo avec traduction complète FR basé sur lightdm-gtk-greeter-settings/1.2.2-3/po/fr.po/
 * Ajout du fichier lightdm-gtk-greeter-settings.patch (informations sur ce fichier)
 * Modification de l'extension .info en .patch des fichiers software-properties.info package-update-indicator.info  gnome-packagekit.info
 * Ajout d'un fond d'écran léger debianr-by-atapaz.jpg basé sur debian-by-atapaz.png pour affichage par défaut et modification du fichier xfce4-desktop.xml
 * Ajout du fichier config/includes.chroot/etc/skel/.config/mimeapps.list pour FileRoller
 * Correction de /config/package-lists/dflinuxe.list.chroot pour le paquet clementine (10/10/20 clementine MIGRATED to testing)
 * Correction sur le fichier dflinuxe11_desktop0.ods pour le paquet clementine 
 * Suppression du paquet lightning-l10n-fr dans dflinux.list.chroot à cause de la nouvelle version 78 de Thunderbird
 * Ajout du lanceur peertube.desktop dans le dossier /Vidéos pour accéder à la page Web https://video.tedomum.net/video-channels/debian.facile_channel/videos
 * Ajout de l'icône dfl-icons-peertube dans /config/includes.chroot/usr/share/pixmaps
 * Suppression du fichier config/includes.chroot/etc/skel/.xscreensaver inutile
 * Ajout par sécurité de config/hooks/normal/workaround-lightlocker.chroot pour désactiver le lancement automatique de light-locker 
 * Modification de dflinuxe11_arborescence.txt
 * Modification de DFbuild.sh par remplacement du texte dflinuxe11 par dflinux et modification dflinuxe.log
 * Modification de /auto/build dflinuxe.log
 * Remplacement des images backgrounds/xfce/Debian11-login.png, backgrounds/xfce/Debian11-login-live.png, isolinux/splash-grub.png, isolinux/splash.png , graphics/logo_debian.png, Debian11.svg de la VERSION 11.0-a2b2 basées sur E-legance_b_Debian_10 et SynchronizedMotions par gradientMotion (https://wiki.debian.org/DebianArt/Themes/gradientMotion)
 * Modification du fichier Debian11_svg.txt
 * Modification et suppression (11 fichiers supprimés) des .desktop dans le dossier /config/includes.chroot/usr/share/applications/ 
 
###### 20/10/2020 - VERSION 11.0-a2b3
 * Modification DFbuild.sh VERSION="11.0-a2b3"
 
###### 27/09/2020 - VERSION 11.0-a2b2 
 * Modification DFbuild.sh VERSION="11.0-a2b2"
 * Modification du fichier dflinux10_arborescence.txt
 * Suppression de dfl-dispatch (/Téléchargements/dfl-dispatch.desktop + /bin/dfl-dispatch + applications/dfl-dispatch.desktop +pixmaps/dfl-dispatch.png)
 * Ajout dans le dossier /Téléchargements du fichier ici_mes_téléchargements
 * Remplacement des fichiers splash-grub.png splash.png basés sur https://0xacab.org/foockinho/E-legance_b_Debian_10_theme/640x480-grub.svg et modifiés en png
 * Remplacement du fichier /config/includes.installer/usr/share/graphics/logo_debian.png basé sur SynchronizedMotions
 * Suppression du fichier /config/includes.chroot/usr/share/backgrounds/xfce/Debian10.svg


###### 25/09/2020 - VERSION 11.0-a2b1
 * [base installateur Debian Bullseye Alpha 2](https://www.debian.org/devel/debian-installer/News/2020/20200316 "Publication de l'installateur Debian Bullseye Alpha 2")
 * Modification de dflinuxe.list.chroot ## Package 'clementine' has no installation candidate (REMOVED from testing)
 * Suppression des fichiers backgrounds/xfce/ dothefunkylinux*.png, dflinux*.png
 * Modification dflinux11_arborescence.txt
 * Activation de l'installation UEFI sans réseau
   * Ajout du fichier config/package-lists/uefi.list.binary
   * Modification auto/config
   * Modification DFbuild.sh
   * Modification auto/config (Typo espace)
   * Modification de config/includes.binary/boot/grub/grub.cfg
   * Suppression de config/includes.binary/install/df-preseed-efi-sda.cfg
   * Modification de config/includes.binary/install/df-preseed-sda.cfg 
   * Modification (Typo) de config/includes.binary/isolinux/live.cfg
   * Modification de config/includes.binary/boot/grub/live-theme/theme.txt
   * Suppression de config/includes.binary/isolinux/splash-efi.png
   * Ajout de config/includes.binary/isolinux/splash-grub.png (1024x768 provisoire) 
   * Modification de config/includes.binary/isolinux/splash.png (640x480 provisoire)
   * Modification de config/includes.binary/isolinux/stdmenu.cfg
 * Ajout du terminal XFCE dans récent de config/includes.chroot/etc/skel/.config/xfce4/panel/whiskermenu-14.rc
 * Désactivation de automount-drives et autobrowse dans xfce4/xfconf/xfce-perchannel-xml/thunar-volman.xml
 * Ajout d'un fichier welcome.conf pour réduire l'espace minimum demandé dans Calamares (requiredStorage à 5 et requiredRam à 0.5)
 * Modification de dflinuxe.list.chroot (## grsync  ## E: Unable to locate package grsync) et reformatage
 * Modification de /usr/lib/live/build/installer_debian-installer: ligne 31 et 321 (suppresion de lilo)

###### 21/08/2020 - VERSION 11.0-a1
 * Modification DFbuild.sh VERSION="11.0-a1"
 * [base installateur Debian Bullseye Alpha 1](https://www.debian.org/devel/debian-installer/News/2019/20191205 "Publication de l'installateur Debian Bullseye Alpha 1")
 * Correction du fichier /auto/config avec dflinuxe11
 * Modification README.md pour dossier /ext (INEXISTANT ici à prévoir éventuellement pour des dépôts externes)
 * Ajout du fuseau Europe/Paris pour installeur Calamares (includes.chroot/etc/calamares/modules/locale.conf)
 * Modification du /doc/dflinux11_arborescence.txt
 * Ajout des applications dans mimeapps.list pour lancement correcte de VLC pour lire les vidéo (Quodlibet se lance si vidéo *.ogg)
 * Suppression du fichier includes.chroot/usr/share/backgrounds/xfce/dflinux9-start.png
 * Remplacement de galculator, cause problème avec le point du clavier numérique en mode papier, par gnome-calculator. Donc modification de dflinux.list.chroot
 * Modification du thème Greybird par Greybird-compact [Style fenetre]
 * Ajout de la ligne user-background = false à /etc/lightdm/lightdm-gtk-greeter.conf pour afficher l'image de fond et donc le mot de passe Debianxx-login plus d'une seconde
 * Ajout du fichier /usr/share/locale/fr/LC_MESSAGES/software-properties.mo (ajout de chaînes FR non traduites otyugh voir software-properties.patch) 
 * Modification du fichier .gitignore (exclusion des fichiers *.info, mais fichiers *.patch autorisés pour les informations des fichiers de langue FR)
 * Ajout du fichier config/hooks/normal/workaround-recovery.chroot pour n'avoir aucun mot de passe à demander en cas mode recovery.
 * Modification de /config/package-lists/dflinux.list.chroot - mtpaint, remplacé pk-update-icon par package-update-indicator
 * Modification des propriétés d'exécution des fichiers /config/hooks/normal/ (pas de chmod -x) 
 * Modification temporaire (Debian10 = Debian11) pour les anciens fichiers du dossier config/includes.chroot/usr/share/backgrounds/xfce/
 * Modification des textes Debian10-login... par Debian11-login... dans late-setup.sh et dflinuxe11_arborescence.txt
 * Modification Icon=debian-reference.png par debian-reference dans .config/xfce4/panel/launcher-1/14724343383.desktop
 * Suppression de l'icône dflinux-welcome.png dans config/includes.chroot/usr/share/pixmaps/dflinux-welcome.png et dans dflinuxe10_arborescence.txt
 * Remplacement des textes dflinux par dflinuxe et aussi pour DFLinux par DFiso si référence à debian-facile dans les fichiers
 * Modification des images splash dans le boot isolinux et modification de stdmenu.cfg
 * Ajout d'un fichier temporaire Debian11.svg basé sur DebianArt/Themes/simpleOutlook
 * Correction et remplacement de Debian10.svg par Debian11.svg dans .config/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml

###### 06/07/2020 - VERSION 11.0-a0
 * Récupération de l'archive de livebuild_dflinuxe10 sur https://framagit.org/Bozosoft/livebuild_dflinuxe10
 * Modification des fichiers pour adaptation Debian 10 vers Debian 11
	* DFbuild.sh VERSION="11.0-a0"
	* build et config  "buster" => bullseye
	* config/includes.binary/boot/grub/live-theme/theme.txt
	* config/includes.binary/boot/grub/grub.cfg
	* config/includes.binary/isolinux/live.cfg
	* config/includes.binary/isolinux/menu.cfg
	* config/includes.chroot/usr/share/dflinux/sources.list
	* config/includes.installer/usr/share/graphics/
	* config/includes.chroot/usr/share/dflinux/bienvenue.html 
	* config/includes.chroot/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/xfwm4.xml et xfce4-panel.xml	
	* config/package-lists/dflinux.list.chroot
		* not available Debian 11 : task-print-server, xfce4-notes-plugin, libreoffice-pdfimport, libreoffice-gtk2, gtk3-engines-xfce
 * Réalisation de la version dflinuxe11-11.0-a0-amd64.iso et tests d'essai
 * Modification du /doc/dflinux11_arborescence.txt
 * Suite au https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=946634, le fichier de traduction FR package-update-indicator.mo (non compris dans le paquet) est maintenant dans le dossier config/includes.chroot/usr/share/locale/fr/LC_MESSAGES/
 * Ajout du dossiers Bureau (.gitkeep = vide à l'installation)


###### 04/07/2020
 * Initialisation du Repository livebuild_dflinuxe11
 * Ajout fichiers CHANGELOG, CONTRIBUTING, README (+ls_bulleyes.pngls_bulleyes.png), TODO, .gitignore


###### Contact
  * [Me contacter](http://jc.etiemble.free.fr/ "Site Web perso")
