** DFLinuxe est un projet personalisé, basé sur ISO Debian-Facile, visant à faciliter l'installation et la prise en main de Debian par les débutants.

** DFLinuxe n'est pas une distribution ou un fork**, c'est une **Debian** agrémentée et configurée.

** Si vous désirez vous tenir au courant et suivre le projet DFLinux conslter le forum  https://debian-facile.org/forum.php et/ou le Wiki https://debian-facile.org/projets:iso-debian-facile

##### Crédits

 - Le site [officiel Debian](https://www.debian.org/ "Debian est un système d'exploitation libre pour votre ordinateur").
 - L'association [Framasoft](https://framasoft.org/ "Changer le monde,un octet à la fois") qui héberge les sources de ces projets 
 - [arpinux](https://arpinux.org "avec une cave bien garnie") : pour la première DFiso et les cahiers du débutant 
 - merci aussi pour la DFiso-Buster à : [Caribou22]( (https://www.facebook.com/quentin.parrain.linux/), [otyugh](https://www.arzinfo.pw/) , [Debian Alain](https://framagit.org/debian-alain)
 - le site [Debian-Facile](https://debian-facile.org "un site menant cette entraide d'une manière toujours plus efficace, joyeuse et réactive !")
 




