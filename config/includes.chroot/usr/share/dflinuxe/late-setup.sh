#!/bin/bash

#Mettre à jour les mirroirs Debian
cp "/usr/share/dflinuxe/sources.list" "/etc/apt/sources.list"

#fix nm profile
rm -f "/etc/NetworkManager/system-connections/Wired connection 1"

#Changer le fond de lightdm en mode live ; sinon nettoyer
LIVEBG="/usr/share/backgrounds/xfce/Debian11-login-live.png"
BG="/usr/share/backgrounds/xfce/Debian11-login.png"
if [ $(df '/' --output=source | tail -n1) == 'overlay' ]; then
	#changer le fond de lightdm
	mv "$LIVEBG" "$BG"
fi

#Configuration de grub
sed -ri 's|^(GRUB_CMDLINE_LINUX_DEFAULT=)"([^"]*)"|\1"\2 splash"|' "/etc/default/grub"
sed -ri 's|^(GRUB_CMDLINE_LINUX=).*|\1"acpi_osi=Linux"|' "/etc/default/grub"
update-grub

#WORKAROUND SALE : l'installation de ce paquet plante si installé via hook (et ajouter le .deb ne résouderai rien - par contre il est pas crucial non plus...)
#apt-get install -y firmware-b43legacy-installer

#Si calamares n'est pas installé, on bazarde le .desktop francisé
hash calamares || rm -f "/usr/share/applications/install-debian.desktop"

#run once ; then autodestory !
systemctl disable late-setup
rm "/etc/systemd/system/late-setup.service"
systemctl daemon-reload
systemctl reset-failed
