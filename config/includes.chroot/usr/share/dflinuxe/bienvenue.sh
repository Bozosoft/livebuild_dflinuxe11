#!/bin/bash
###################


# mise en place des bookmarks idem welcome.sh DF
if ! [ -e /home/$USER/.gtk-bookmarks ]; then
    echo "file:///home/$USER/Documents" > /home/$USER/.gtk-bookmarks
    echo "file:///home/$USER/Images"  >> /home/$USER/.gtk-bookmarks
    echo "file:///home/$USER/Musique"  >> /home/$USER/.gtk-bookmarks
    echo "file:///home/$USER/Public"  >> /home/$USER/.gtk-bookmarks
    echo "file:///home/$USER/T%C3%A9l%C3%A9chargements" >> /home/$USER/.gtk-bookmarks
    echo "file:///home/$USER/Vid%C3%A9os" >> /home/$USER/.gtk-bookmarks
fi

# le fichier bienvenue.html dans le même dossier que bienvenue.sh
FILE=`dirname $0`/bienvenue.html

# message d'accueil Modifié pour 2 cas lancement auto depuis /autostart/ ou lancement depuis /Bureau
# message d'accueil si le fichier est dans autostart
	if [ -f /home/$USER/.config/autostart/bienvenue.desktop ];
           then
    zenity --title "Bienvenue" \
    --text-info --checkbox="Cocher pour NE PAS revoir ce message au prochain démarrage" \
    --cancel-label="Revoir au prochain démarrage" --width=800 --height=500\
    --ok-label "Valider le choix NE PAS revoir" --filename=$FILE  --html
           else
# message d'accueil si le fichier est sur le bureau          
    zenity --title "Bienvenue" --text-info --width=800 --height=500\
    --ok-label "Bonne découverte" --filename=$FILE  --html
           fi
# déplacement du lanceur de l'écran d'accueil sur le bureau si la case "NE PAS revoir..." est cochée
    case $? in
    0)
        if [ -e /home/$USER/.config/autostart/bienvenue.desktop ]; then
          mv /home/$USER/.config/autostart/bienvenue.desktop /home/$USER/Bureau
        fi
        ;;
    esac
    exit 0
    
# end file
    
