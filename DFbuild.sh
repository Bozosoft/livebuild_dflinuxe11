#!/bin/bash
## Base https://debian-facile.org/git/ProjetsDF/dfiso-buster/wiki/guide-live-build
## dflinuxe11 live-build script  ## https://framagit.org/Bozosoft/livebuild_dflinuxe11

# cfg ------------------------------------------------------------------
VERSION="11.11"

if [ `whoami` != root ]; then
    echo "erreur : lancer DFbuild en tant qu'administrateur"
    exit 1
fi
# args -----------------------------------------------------------------
if [ "$1" == "32" ]; then ARCH="i386"
elif [ "$1" == "64" ]; then ARCH="amd64"
elif [ "$1" == "clean" ]; then lb clean && rm -R -f cache && exit 0
else
    # no args or wrong args > help
    echo "Utilisation : en mode administrateur"
    echo "./DFbuild.sh 32    > construire dflinuxe11 i386"
    echo "./DFbuild.sh 64    > construire dflinuxe11 amd64"
    echo "./DFbuild.sh clean > nettoyer le dossier de construction et le cache"
    echo "---"
    echo "prenez soin de nettoyer le cache avant de construire sous une nouvelle architecture."
    exit 1
fi

## build ISO -----------------------------------------------------------

## place nette
lb clean

## mise en place
echo "INFO: building dflinuxe-${VERSION}-${ARCH}"

# dossier des paquets externes
## rm config/packages/*
## ln ext/*.deb config/packages/

# import des paquets externes
## cp ext/*.deb config/packages/

# utiliser apt-cacher s'il est installé # maj 24/09/20
if dpkg-query -W apt-cacher-ng &>"/dev/null"
then
	export http_proxy="http://127.0.0.1:3142/"
	CONFIG+=("--apt-http-proxy" "$http_proxy")
else
	echo "WARNING: apt-cacher-ng n'est pas installé"
fi

# architecture handling # maj 24/09/20
CONFIG+=("-a" "${ARCH}")
case $ARCH in
	i386)CONFIG+=("-k" "686" "--bootloader"  "syslinux");;
	amd64)CONFIG+=("--bootloader"  "syslinux,grub-efi");;
esac

# version de la clef USB ## dflinuxe-11.x-i386 ou dflinuxe-11.x-amd64
CONFIG+=("--iso-application" "dflinuxe_$VERSION" "--iso-volume" "dle_$VERSION-${ARCH}")

# live-build time
lb config ${CONFIG[@]}
lb build


## renommage
if test -f live-image-${ARCH}.hybrid.iso; then
	echo "INFO: renommer"
    mkdir -p dfl-${ARCH}
    mv live-image-${ARCH}.hybrid.iso dfl-${ARCH}/dflinuxe-${VERSION}-${ARCH}.iso
    mv chroot.packages.install dfl-${ARCH}/dflinuxe-${VERSION}-${ARCH}.pkgs
    mv dflinuxe.log dfl-${ARCH}/dflinuxe-${VERSION}-${ARCH}.log
    cd dfl-${ARCH} && md5sum dflinuxe-${VERSION}-${ARCH}.iso > dflinuxe-${VERSION}-${ARCH}.md5 && cd ../
    lb clean
    echo "Opération achevée en $(($SECONDS/60)) minutes"
else
    echo "ISO non construite : erreur, voir le fichier dflinuxe.log"
    exit 1
fi

exit 0
