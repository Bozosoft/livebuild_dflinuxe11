
### Créer sa propre image ISO Debian 11 XFCE

 - DFLinuxe est un projet personnalisé, basé sur le projet DF (Debian-Facile), visant à faciliter l'installation et la prise en main de Debian par les débutants.
 - Pour information   [ProjetsDF](https://debian-facile.org/git/ProjetsDF "Debian-Facile Un service Git")  ou  [arpinux.org](https://arpinux.org/ "je rassemble ici mes activités numériques...") et 
 [le guide live build](https://debian-facile.org/git/ProjetsDF/dfiso-buster/wiki/guide-live-build "Construire un Live Debian")

 * Installez les paquets live-build, live-tool, tree et live-manual (si besoin)
 * Installez la version disponible dans les dépôts si vous construisez une ISO de la même version.
   - Si par exemple vous êtes actuellement sous Debian 10 et que vous voulez construite une ISO de Debian 11 récupérer les fichiers de la version future

- A : [Paquet live-build](https://packages.debian.org/fr/bullseye/live-build "composants de construction de système autonome")
  - Note il faudra aussi prendre les paquets debootstrap et arch-test
  - [Paquet debootstrap](https://packages.debian.org/fr/bullseye/debootstrap "Installation d'un système Debian de base")
  - [Paquet arch-test](https://packages.debian.org/fr/bullseye/arch-test "détection des architectures gérées par une machine et son noyau")
- B : [Paquet live-tools](https://packages.debian.org/fr/bullseye/live-tools "Live System Extra Components")
 - Donc installez les paquets 1- arch-test, 2- debootstrap, 3- live-build, 4- live-tools
  
 * Créer par exemple un dossier /home/user/tmp/dfiso
 * Récupérer les sources depuis ce dépôt et décompressez les fichiers dans votre dossier /dfiso11 afin d'obtenir la bonne arborescence 
	- dfiso/auto
	- dfiso/config
	- dfiso/doc
	- dfiso/DFbuild.sh
	
#### le fichier DFbuild.sh 
- permet de nettoyer les dossiers des sources sur sa machine par ./DFbuild.sh clean
- permet de construire un fichier ISO 32 bits ou 64 bits par la commande ./DFbuild.sh 32 ou ./DFbuild.sh 64 

#### Donc ouvrez un terminal et lancez la commande ./DFbuild.sh... pour construire l'ISO
- Une fois le process lancé vous pouvez suivre le déroulement des étapes sur le terminal. 
	A la fin de la construction vous pourrez lire [20XX-XX-XX 16:57:37] lb clean noauto 
	P: Cleaning chroot
	Opération achevée en XX minutes
- Selon votre machine et la qualité de votre réseau Internet, cela peut prendre entre un certain temps.

- De plus le système génère un fichier des opérations de construction exécutés (dflinuxexx.log)
- l'arborescence à la fin du traitement ressemblera à 
	- dfiso/.build
	- dfiso/auto
	- dfiso/cache
	- dfiso/config
	- dfiso/dfl-amd64 ->dossier si ISO 64 bits
	- dfiso/dfl-i386  ->dossier si  ISO 32 bits
	- dfiso/doc
	- dfiso/ext (INEXISTANT ici à prévoir éventuellement pour des dépôts externes)
	- dfiso/DFbuild.sh

##### A la fin du processus les données de l'ISO sont placées dans le dossier dfl-i386 ou dfl-amd64 (ISO 32 bits ou 64 bits)
- avec les fichiers suivants
	- dflinuxexxxxx.md5 : l'empreinte numérique du fichier *.iso
	- dflinuxexxxxx.log : les informations de construction
	- dflinuxexxxxx.iso : le fichier *.iso distribution construite
	- dflinuxexxxxx.pkgs : les noms des paquets de la distribution construite 

##### Nota : si vous construisez plusieurs fois l'ISO en 64 Bits inutile de vider le cache vous allez gagner de nombreuses minutes sur le processus

##### Il ne vous reste plus qu’à tester votre distribution dérivée Debian personnalisée et y apporter d'eventuel correctifs pour reconstruire une version plus finalisée.

Si vous désirez vous tenir au courant et suivre le projet ISO Debian-Facile consulter le forum  https://debian-facile.org/forum.php et/ou le Wiki https://debian-facile.org/projets:iso-debian-facile

##### Crédits

 - Le site [officiel Debian](https://www.debian.org/ "Debian est un système d'exploitation libre pour votre ordinateur").
 - L'association [Framasoft](https://framasoft.org/ "Changer le monde,un octet à la fois") qui héberge les sources de ces projets 
 - [arpinux](https://arpinux.org "avec une cave bien garnie") : pour la première DFiso et les cahiers du débutant 
 - merci aussi pour la DFiso-Buster à : [Caribou22]( (https://www.facebook.com/quentin.parrain.linux/), [otyugh](https://www.arzinfo.pw/) , [Debian Alain](https://framagit.org/debian-alain)
 - le site [Debian-Facile](https://debian-facile.org "un site menant cette entraide d'une manière toujours plus efficace, joyeuse et réactive !")

 * [Me contacter](http://jc.etiemble.free.fr/ "Site Web perso")
